.. socketcan-xcp documentation master file, created by
   sphinx-quickstart on Thu Jul  7 21:01:27 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to socketcan-xcp's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage/api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
