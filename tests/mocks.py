""" module:: tests.mocks
    :platform: Any
    :synopsis: Mock Objects for use in tests
    moduleauthor:: Patrick Menschel (menschel.p@posteo.de)
    license:: GPL v3
"""
from typing import Tuple, Union

from socketcan_xcp.protocol import StdCmd, parse_packet_from_client
from socketcan_xcp.server import XcpServer
from socketcan_xcp.transport import XcpTransport, XcpOnCan
from queue import Queue


class MockTransport(XcpTransport):
    """
    The mock is a simple queue construction
    so tests can read from / write to the queues
    to mimic a transport
    """

    def __init__(self):
        super().__init__()
        self.rx_queue = Queue()
        self.tx_queue = Queue()

    def send(self, data: bytes) -> int:
        self.tx_queue.put(data)
        return len(data)

    def recv(self) -> bytes:
        return self.rx_queue.get()

    def get_tx_message(self):
        return self.tx_queue.get()

    def put_rx_message(self, data):
        return self.rx_queue.put(data)


def get_queued_transport_tuple() -> Tuple[MockTransport, MockTransport]:
    transport1 = MockTransport()
    transport2 = MockTransport()
    transport1.rx_queue = transport2.tx_queue
    transport2.rx_queue = transport1.tx_queue
    return transport1, transport2


def get_xcp_on_can_pair(interface="vcan0", tx_can_id=0x300, rx_can_id=0x400) -> Tuple[XcpOnCan, XcpOnCan]:
    transport1 = XcpOnCan(interface=interface, tx_can_id=tx_can_id, rx_can_id=rx_can_id)
    transport2 = XcpOnCan(interface=interface, tx_can_id=rx_can_id, rx_can_id=tx_can_id)
    return transport1, transport2


class MockServer(XcpServer):
    """
    A Xcp Server that can be set to cause timeout, errors etc.
    """

    def __init__(self, transport: Union[XcpTransport, XcpOnCan], endianess):
        super().__init__(transport, endianess)
        self._cause_timeout_cnt = 0

    def cause_timeout(self, cnt: int):
        self._cause_timeout_cnt = cnt

    def handle_rx(self):
        """
        The thread handling incoming communication.

        It reacts on the commands from a client.
        :return: Nothing.
        """
        while True:
            data = self.transport.recv()
            req = parse_packet_from_client(data=data, endianess=self.endianess)
            command = req.get("packet_id")
            handler = self.cmd_handler_mapping.get(command)
            if handler is not None and callable(handler):
                if self._cause_timeout_cnt > 0:
                    self._cause_timeout_cnt -= 1
                else:
                    handler(req)
            else:
                self._logger.error("Unhandled Command {0}".format(command.name))
