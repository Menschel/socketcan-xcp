# users
# a linux system with either VCAN0 or a real CAN device
socketcan

# developers - additional
tox
pytest
pytest-cov
flake8<5
pytest-flake8
