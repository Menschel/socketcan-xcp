from typing import Tuple

import pytest

from tests.mocks import get_queued_transport_tuple, get_xcp_on_can_pair, MockServer
from socketcan_xcp.client import XcpClient
from socketcan_xcp.protocol import ComModeOptional, ResourceFlag, ComModeBasicFlag, ProtectionStatus, ConnectMode, \
    PacketIdFromServer
from socketcan_xcp.transport import XcpTransport


@pytest.fixture(scope="session", params=[get_queued_transport_tuple, get_xcp_on_can_pair])
def transport(request) -> Tuple[XcpTransport, XcpTransport]:
    try:
        transport = request.param()
    except OSError:
        pytest.skip("Skip XcpOnCan - No VCAN0 available")
    else:
        yield transport


@pytest.fixture(scope="session")
def mock_client_server_pair(transport) -> Tuple[XcpClient, MockServer]:
    server = MockServer(transport=transport[0], endianess="little")
    client = XcpClient(transport=transport[1])
    server._resource = ResourceFlag(0x15)
    server._com_mode_basic = ComModeBasicFlag(0xC0)
    server._max_cto = 8
    server._max_dto = 8
    server._com_mode_optional = ComModeOptional(1)
    server._max_bs = 2
    server._min_st = 0
    server._queue_size = 0
    server._xcp_driver_version = 6.4

    server._protection_status = (ProtectionStatus.CalibrationAndPagingIsProtected
                                 | ProtectionStatus.DaqIsProtected
                                 | ProtectionStatus.ProgrammingIsProtected
                                 )
    yield client, server


@pytest.fixture(scope="class", params=ConnectMode)
def connect_mode(request):
    yield request.param


@pytest.fixture(scope="class", params=PacketIdFromServer)
def packet_id_from_server(request):
    yield request.param


@pytest.fixture(scope="class", params=["big", "little"])
def endianess(request):
    yield request.param
